class Pagination(object):

    def __init__(self, request,current_page_num, all_count,per_page_num=10, pager_count=11):
        """
        封装分页相关数据
        :param current_page_num: 当前访问页的数字
        :param all_count:    数据库中的数据总条数
        :param per_page_num: 每页显示的数据条数
        :param pager_count:  最多显示的页码个数
        """

        try:
            current_page_num = int(current_page_num)
        except Exception as e:
            current_page_num = 1

        if current_page_num < 1:
            current_page_num = 1

        self.current_page_num = current_page_num

        self.all_count = all_count
        self.per_page_num = per_page_num
        self.request = request

        # 保存搜索条件
        import copy

        self.params = copy.deepcopy(request.GET)    # 拷贝过后的数据是可以更改的
        '''
        self.params['xx'] = 123
        print(self.params)      # page=1&a=1&xx=123
        print("GET",request.GET)
        print(self.params.urlencode())
        '''

        # 总页码
        all_pager, tmp = divmod(all_count, per_page_num)
        if tmp: # 如果tmp不为0,总页数就加1
            all_pager += 1
        self.all_pager = all_pager
        print("all_count",self.all_count)

        self.pager_count = pager_count
        self.pager_count_half = int((pager_count - 1) / 2)  # 5

    @property
    def start(self):
        return (self.current_page_num - 1) * self.per_page_num

    @property
    def end(self):
        # if self.all_pager < self.current_page_num * self.per_page_num:
        #     return self.all_pager
        return self.current_page_num * self.per_page_num

    # 渲染页码标签
    def page_html(self):
        # 如果总页码 < 11个，显示全部页码
        if self.all_pager <= self.pager_count:
            pager_start = 1
            pager_end = self.all_pager + 1  # +1 是因为range 顾头不顾尾
        # 总页码  > 11，显示前5后5
        else:
            # 当前页如果<=页面上最多显示11/2个页码
            if self.current_page_num <= self.pager_count_half:
                pager_start = 1
                pager_end = self.pager_count + 1

            # 当前页大于5
            else:
                # 页码翻到最后        # 当前页+5大于总页码，显示最有一页+1
                if (self.current_page_num + self.pager_count_half) > self.all_pager:
                    pager_end = self.all_pager + 1
                    pager_start = self.all_pager - self.pager_count + 1
                else:
                    pager_start = self.current_page_num - self.pager_count_half
                    pager_end = self.current_page_num + self.pager_count_half + 1

        page_html_list = []

        first_page = '<li><a href="?page=%s">首页</a></li>' % (1,)
        page_html_list.append(first_page)

        if self.current_page_num <= 1:  # 当前选中的页小于等于1
            prev_page = '<li class="disabled"><a href="#">上一页</a></li>' # 禁用样式
        else:
            prev_page = '<li><a href="?page=%s">上一页</a></li>' % (self.current_page_num - 1,)

        page_html_list.append(prev_page)



        for i in range(pager_start, pager_end):

            self.params['page'] = i     # 把page的值改为当前页
            if i == self.current_page_num:  # 当前选中页
                temp = '<li class="active"><a href="?%s">%s</a></li>' % (self.params.urlencode(), i,)
            else:
                temp = '<li><a href="?%s">%s</a></li>' % (self.params.urlencode(), i,)
            page_html_list.append(temp)


        # 判断有无下一页
        if self.current_page_num >= self.all_pager:
            next_page = '<li class="disabled"><a href="#">下一页</a></li>'
        else:
            next_page = '<li><a href="?page=%s">下一页</a></li>' % (self.current_page_num + 1,)
        page_html_list.append(next_page)

        last_page = '<li><a href="?page=%s">尾页</a></li>' % (self.all_pager,)
        page_html_list.append(last_page)

        return ''.join(page_html_list)