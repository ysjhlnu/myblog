from django.shortcuts import render,HttpResponse,redirect
from blog.models import Category, Article, Banner, Tui, Tag, Link
from blog.pagination import Pagination


def global_variable(request):
    allcategory = Category.objects.all()
    remen = Article.objects.filter(tui__id=2)[:6]
    tags = Tag.objects.all()
    return locals()


# 首页
def index(request):
    article_list = Article.objects.all().order_by("-modified_time")  # 根据修改时间来排序
    print(article_list)
    current_page_num = request.GET.get('page', 1)
    print("current_page_num", current_page_num)

    pagination = Pagination(request, current_page_num, article_list.count())
    article_list = article_list[pagination.start:pagination.end]
    print("start:", pagination.start, "end", pagination.end)

    # 目录
    category_list = Category.objects.all()

    # 幻灯片
    banner_list = Banner.objects.filter(is_active=True)[0:4]  # 查询所有幻灯图数据，并进行切片

    # 标签
    tags_list = Tag.objects.all()

    # 推荐位
    tui_list = Tui.objects.all()

    # 友情链接
    link_list = Link.objects.all()

    # 推荐位文章
    hot_list = article_list[:11]  # 前10篇文章

    return render(request, 'index.html', {"link_list": link_list, "tags_list": tags_list, "article_list": article_list,
                                          "category_list": category_list,
                                          "banner_list": banner_list, "tui_list": tui_list, "hot_list": hot_list})


# 列表页
def list(request, lid):
    list = Article.objects.filter(category_id=lid)  # 获取通过URL传进来的lid，然后筛选出对应文章
    cname = Category.objects.get(id=lid)  # 获取当前文章的栏目名
    reman = Article.objects.filter(tui__id=2)
    category_list = Category.objects.all()  # 导航所有分类
    tags_list = Tag.objects.all()  # 右侧所有文章标签

    return render(request, 'list.html', locals())


# 内容页
def show(request, sid):
    show = Article.objects.get(pk=sid)  # 查询指定ID的文章
    print(show)
    category_list = Category.objects.all()  # 导航上的分类
    tags_list = Tag.objects.all()  # 右侧所有标签
    remen = Article.objects.filter(tui__id=2)[:6]  # 右侧热门推荐
    hot_list = Article.objects.all().order_by('?')[:5]  # 内容下面的您可能感兴趣的文章，随机推荐
    previous_blog = Article.objects.filter(created_time__gt=show.created_time, category=show.category.id).first()
    netx_blog = Article.objects.filter(created_time__lt=show.created_time, category=show.category.id).last()
    show.views = show.views + 1
    show.save()

    # 文章的浏览数，我们先通过show.views查询到当前浏览数，然后对这个数进行加1操作，意思是每访问一次页面（视图函数），就进行加1操作。然后再通过show.save()
    # 进行保存。
    return render(request, 'show.html', locals())


# 关于我们
def about(request):
    return render(request, 'page.html', locals())


# 标签页
def tag(request, tag):
    list = Article.objects.filter(tags__name=tag)  # 通过文章标签进行查询文章
    remen = Article.objects.filter(tui__id=2)[:6]
    allcategory = Category.objects.all()
    tname = Tag.objects.get(name=tag)  # 获取当前搜索的标签名
    page = request.GET.get('page')
    tags = Tag.objects.all()
    paginator = Paginator(list, 5)
    try:
        list = paginator.page(page)  # 获取当前页码的记录
    except PageNotAnInteger:
        list = paginator.page(1)  # 如果用户输入的页码不是整数时,显示第1页的内容
    except EmptyPage:
        list = paginator.page(paginator.num_pages)  # 如果用户输入的页数不在系统的页码列表中时,显示最后一页的内容
    return render(request, 'tags.html', locals())


# 搜索页
def search(request):
    ss = request.GET.get('search')
    list = Article.objects.filter(title__contains=ss)
    page = request.GET.get('page')
    paginator = Paginator(list, 10)
    try:
        list = paginator.page(page)  # 获取当前页码的记录
    except PageNotAnInteger:
        list = paginator.page(1)  # 如果用户输入的页码不是整数时,显示第1页的内容
    except EmptyPage:
        list = paginator.page(paginator.num_pages)  # 如果用户输入的页数不在系统的页码列表中时,显示最后一页的内容
    return render(request, 'search.html', locals())
