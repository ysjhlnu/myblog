from django.template import Library
from blog.models import Category,Article

register = Library()

@register.inclusion_tag('base.html')
def show_category():
    category_list = Category.objects.all()
    return {"category_list":category_list}

@register.inclusion_tag('right.html')
def hot():
    hot_list = Article.objects.all().order_by("-modified_time")["6"]
    return {"hot_list":hot_list}